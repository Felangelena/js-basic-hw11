"use strict";

const form = document.querySelector("#passwordForm");

function changeIcon (icon) {
    icon.classList.toggle("fa-eye");
    icon.classList.toggle("fa-eye-slash");
}

form.addEventListener('mousedown', e => {
    if(e.target.closest("i")) {
        changeIcon(e.target);
        e.target.previousElementSibling.setAttribute("type", "text");
    }
})

form.addEventListener('mouseup', e => {
    if(e.target.closest("i")) {
        changeIcon(e.target);
        e.target.previousElementSibling.setAttribute("type", "password");
    }
})

const btnConfirm = document.querySelector("#confirm");
const inputs = document.querySelectorAll(".password");

btnConfirm.addEventListener('click', e => {
    e.preventDefault();

    function createWarning () {
        if (document.getElementById("warning") == null) {
            const p = document.createElement("p");
            p.textContent = "Потрібно ввести однакові значення";
            p.style.color = "red";
            p.setAttribute("id","warning");
            inputs[1].parentElement.append(p);
        }
    }

    function deleteWarning () {
        document.getElementById("warning").remove();
    }

    if (inputs[0].value == "" || inputs[1].value == "") {
        createWarning();
    } else if (inputs[0].value === inputs[1].value) {
            deleteWarning ();
            alert("You are welcome");
        } else {
                createWarning();
            }
})



